// Deletor.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include "TestList.h"
#include "csprng.h"

void ListDirectoryFiles(TCHAR *directory, List* list) {
	HANDLE handle;
	WIN32_FIND_DATA file;

	TCHAR searchString[100];
	wcscpy(searchString, directory);
	wcscat(searchString, L"*");
	TCHAR filepath[MAX_PATH];

	handle = FindFirstFile(searchString, &file);
	searchString[wcslen(searchString) - 1] = 0;

	if (handle != INVALID_HANDLE_VALUE) {
		FindNextFile(handle, &file);
		while (FindNextFile(handle, &file) != 0) {
			wcscpy(filepath, directory);
			wcscat(filepath, file.cFileName);
			prepend_list(list, &filepath);
		}
	}
}

int main(int argc, char *argv[])
{
	int status;
	long msPause = 0;
	List testList;
	CSPRNG rng = NULL;
	init_list(&testList);
	unsigned long randomDelay = 0;

	if (argc > 1) {
		errno = 0;
		msPause = strtol(argv[1], NULL, 10);
		if (errno != 0) {
			printf("Error! Please enter only number values in ms.");
			return -1;
		}
	}

	if (msPause > 20000)
		msPause = 0;

	rng = csprng_create(rng);
	if (!rng)
		return 1;

	TCHAR base[100] = L"C:\\ProggerTests";
	status = CreateDirectoryW(base, NULL);
	wcscat(base, L"\\Deleter");
	status = CreateDirectoryW(base, NULL);
	wcscat(base, L"\\");
	ListDirectoryFiles(base, &testList);

	for (int i = 0; i < 10000; i++) {
		if (msPause == -1) {
			randomDelay = csprng_get_int(rng);
			randomDelay %= 5001;
			Sleep(randomDelay);
		}
		else
			Sleep(msPause);

		if (list_size(&testList) > 0) {
			int myInt = rand() % list_size(&testList);
			ListNode *tmp = select_node(&testList, myInt);
			DeleteFile(tmp->filename);
			remove_any(&testList, tmp);
		}
		else {
			break;
		}
	}

    return 0;
}

